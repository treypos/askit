class RemoveUserIdFromQuestionsAnswers < ActiveRecord::Migration[7.0]
  def up
    change_column_default :questions, :user_id, from: User.second.id, to: nil
    change_column_default :answers, :user_id, from: User.second.id, to: nil
  end

  def down
    change_column_default :questions, :user_id, from: nil, to: User.second.id
    change_column_default :answers, :user_id, from: nil, to: User.second.id
  end
end
