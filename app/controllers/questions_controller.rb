class QuestionsController < ApplicationController
  before_action :set_question, only: %i[show destroy edit update]
  before_action :fetch_tags, only: %i[new edit]
  def show
    @question = @question.decorate
    @answer = @question.answers.build
    @answers = @question.answers.order(created_at: :desc)
    @answers = @answers.decorate
  end

  def destroy
    @question.destroy
    flash[:success] = t('questions.destroy.success')
    redirect_to questions_path
  end

  def edit; end

  def update
    if @question.update question_params
      flash[:success] = t('questions.update.success')
      redirect_to questions_path
    else
      render :edit
    end
  end

  def index
    @pagy, @questions = pagy Question.all_by_tags(params[:tag_ids])
    @questions = @questions.decorate
  end

  def new
    @question = Question.new
  end

  def create
    @question = current_user.questions.build question_params

    if @question.save
      flash[:success] = t('questions.create.success')
      redirect_to questions_path
    else
      render :new
    end
  end

  private

  def question_params
    params.require(:question).permit(:title, :body, tag_ids: [])
  end

  def set_question
    @question = Question.find params[:id]
  end

  def fetch_tags 
    @tags = Tag.all
  end
end
